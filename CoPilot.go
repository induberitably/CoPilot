package main

import (
	"bufio"
	"bytes"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"math/rand"
	"os"
	"os/exec"
	_ "reflect"
	"strconv"
	"time"
)

type Person struct {
	Id     int
	Active bool
	Name   string
}

func phantom(seed int) {
	sseed := strconv.Itoa(seed)
	a := "calendar.js"
	cmd := exec.Command("phantomjs", a, sseed)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

}
func read_name() string {
	f := bufio.NewWriter(os.Stdout)
	defer f.Flush()
	f.Flush()
	a := "FindName.rb"
	cmd := exec.Command("ruby", a)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	output := out.String()
	return output
}
func main() {
	gen_id := generate_id(100000, 999999)
	if gen_id <= 100000 {
		log.Fatal("...")
	}
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	defer session.Close()
	c := session.DB("PILOT").C("id")
	if err != nil {
		panic(err)
	}

	/*generate id
	  query id on pilot
	  if pilot returns error don't write
	  else write to pilot
	*/
	if !id_exists(gen_id, c) {
		phantom(gen_id)
		a := read_name()
		if a != "-1" {
			mongo_populate_id(a, gen_id, true, c)
			fmt.Println(a)
		}
	}
}

func id_exists(gen_id int, c *mgo.Collection) bool {
	result := Person{}
	c.Find(bson.M{"id": gen_id}).One(&result)
	fmt.Println(result.Name)
	return result.Id == gen_id
}
func generate_id(min, max int) int {

	rand.Seed(time.Now().Unix())

	return rand.Intn(max-min) + min
}
func mongo_populate_id(name string, id int, active bool, c *mgo.Collection) {
	c.Insert(&Person{id, active, name})

}
