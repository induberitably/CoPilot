# Summary
In summer of 2017 [I accidentally broke into Wright State University's course system](https://drive.google.com/file/d/0B5OoiQ6joF_aTEcxVU9SUzVYV28/view?usp=sharing&resourcekey=0-JK-SS45JP9Q4UmtdJtyIlw) and wrote code to spoof my way into an admin account. 

This code's end goal is now defunct as they've patched the bug, but this should encourage folks to learn how to use PhantomJS - for scraping websites, Ruby - I enjoy Ruby for the gems for parsing html and regex, MongoDB - if this isn't the simplest but most powerful database out there, and Go - the language that I used to tie all these pieces together. 
